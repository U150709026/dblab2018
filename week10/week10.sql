show columns from customers;

select * from customers
order by CustomerName;

create index newindex
on customers ( CustomerName );

select * from customers;

explain select * from customers;

alter table customers drop index newindex;

create view tmp as select * from customers; -- view = virtual table
select * from tmp;