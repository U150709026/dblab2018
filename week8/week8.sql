update customers
set ContactName = "Ahmet Onder Gur"
where CustomerID = 3 ;

update customers
set ContactName = "Ahmet Onder Gur" , City = "Muğla"
where Country = "Argentina" ;

select OrderID
from orders
where CustomerID = 3 ;

delete from orderdetails
where OrderID = 10365 ;

delete from orders
where CustomerID = 3;

delete from customers
where CustomerID = 3 ;

select city
from customers
union
select city
from suppliers;
