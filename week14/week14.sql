-- trigers 

CREATE TABLE `employees_backup` (
	`id` INT auto_increment,
    `EmployeeID` int(11) NOT NULL,
    `LastName` varchar(45) NOT NULL,
    `FirstName` varchar(45) NOT NULL,
    `BirthDate` varchar(45) NOT NULL,
    `changedat` DATETIME DEFAULT NULL,
    `action` varchar(50) DEFAULT NULL,
    PRIMARY KEY (`id`)
);

select * from employees_backup;
select * from employees;

update employees set LastName = 'Gur'
where EmployeeID = 6;

update employees set BirthDate = "1999-01-01" where EmployeeID = 8;

-- CREATE DEFINER=`root`@`localhost` TRIGGER `company`.`employees_AFTER_INSERT` AFTER INSERT ON `employees` FOR EACH ROW
-- BEGIN
-- 	INSERT INTO employees_backup
--     SET action = 'insert',
-- 		EmployeeID = NEW.EmployeeID,
--         LastName = NEW.LastName,
--         FirstName = NEW.FirstName,
--         BirthDate = NEW.BirthDate,
--         changedat = NOW();
-- END

insert into employees (EmployeeID,LastName, FirstName, BirthDate, Photo, Notes)
	values ("123", "Gur", "Ahmet", "1997-06-12", "EmpID11.pic", "Intern");

delete from employees
where EmployeeID = "123";