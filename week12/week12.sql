ALTER TABLE employees
ADD country varchar(100);

ALTER TABLE employees
ADD bDate date;

ALTER TABLE employees
modify column bDate year;

ALTER TABLE employees
drop column country;

truncate table orderdetails;